# Real-time Currency Tracker

## Installation and startup
#### You must have the following installed:

- `Node.js` v18.17.0 and upper
- `yarn` v1.22.17 and upper

#### To run, in the project directory:

Dependency setting:
#### `yarn` or `npm install`
To start:
#### `yarn dev` or `npm run dev`
To run tests
#### `yarn test` or `npm run test`

## Description of the project architecture and selected technologies

#### Architecture
To create this project, I chose [**Feature Sliced Design**](https://feature-sliced.design/) architecture. In my opinion, this is the most convenient approach for creating scalable and maintainable projects.

Its point is to divide the application code into layers and then into slices.
The layers are standardized across all projects and vertically arranged. Modules on one layer can only interact with modules from the layers strictly below.

In the standard architecture, there are 6 layers:

- **app** — app-wide settings, styles and providers.
- **pages** — compositional layer to construct full pages from entities, features and widgets.
- **widgets** — compositional layer to combine entities and features into meaningful blocks. (e.g. IssuesList, UserProfile).
- **features** — user interactions, actions that bring business value to the user. (e.g. SendComment, AddToCart, UsersSearch).
- **entities** — business entities. (e.g., User, Product, Order).
- **shared** — reusable functionality, detached from the specifics of the project/business. (e.g. UIKit, libs, API)/.

Then there are slices, which partition the code by business domain. This makes your codebase easy to navigate by keeping logically related modules close together. Slices cannot use other slices on the same layer, and that helps with high cohesion and low coupling.

In my application, to save time, I combined the layers: entity, features and widgets into one adn called it widgets.

###
#### Vite
To create the initial application template, I used **Vite** instead of **CRA** because Vite is much faster while still having all the features that CRA has.

###
#### State management
In this functionality, global state was not needed. Perhaps I should have imagined that I was developing a large application and created a global state right away, but there is nowhere to use it for the current functionality. 
I store and retrieve the currency name from the query parameters. And the price data is processed in the local state. But if I needed to add global storage, I would use mobx.

###
#### Styling
For styling in the app, I used emotion. It provides more flexibility than styled-components. And at the same time it uses css-in-js. It can be used either in the usual styled-components style or with a css attribute.
In my opinion, the css-attribute approach allows you to write cleaner and more readable code. Because with this approach, when you first look at the code, it is immediately clear where is a react component and where is just a styled component.
Also, the css-attribute approach is much easier to extend and reuse styles.

###
#### UI Kit
I used Ant Design as my UIKit because in my experience, it's more comfortable to work with than the more widely known Material UI. And Design is not inferior in its component library.

###
#### API
As an http-client, I always use axios. It provides a more convenient API to work with than fetch.
