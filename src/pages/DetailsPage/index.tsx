import { HistoricalPricePanel } from "widgets/historical-price-panel";
import { CurrentRate } from "widgets/current-rate";
import { pageContainerStyles } from "pages/DetailsPage/styles";


const DetailsPage = () => {
	return (
		<div css={pageContainerStyles}>
			<CurrentRate />
			<HistoricalPricePanel />
		</div>
	);
};

export default DetailsPage;