import { css } from "@emotion/react";

export const pageContainerStyles = css`
  display: flex;
	gap: 1rem;
	align-items: flex-start;
`;