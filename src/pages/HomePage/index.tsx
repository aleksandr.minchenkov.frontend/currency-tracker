import React from 'react';
import { CommodityList } from "widgets/available-commodity";

const HomePage = () => {
	return (
		<React.Fragment>
			<h1>List of available commodities</h1>
			<CommodityList />
		</React.Fragment>
	);
};

export default HomePage;