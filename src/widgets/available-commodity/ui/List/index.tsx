import { listContainerStyles } from "./styles";
import { LinkCard } from "shared/ui";
import { PAGE_URLS } from "shared/data";
import { availableCommodities } from "../../model/consts";

const CommodityList = () => {
	return (
		<div css={listContainerStyles}>
			{availableCommodities.map(value => <LinkCard key={value} label={value} link={`${PAGE_URLS.details}?stock=${value}`} />)}
		</div>
	);
};

export default CommodityList;