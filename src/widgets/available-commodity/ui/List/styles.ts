import { css } from "@emotion/react";

export const listContainerStyles = css`
  display: flex;
  flex-direction: column;
  gap: 1rem;
	align-items: center;
`;
