export enum IntervalEnum {
	'1min' = '1min',
	'5min' = '5min',
	'15min' = '15min',
	'30min' = '30min',
	'45min' = '45min',
	'1h' = '1h',
	'2h' = '2h',
	'4h' = '4h',
	'1day' = '1day',
	'1week' = '1week',
	'1month' = '1month',
}