export interface HistoryItem {
	datetime: string;
	open: number;
	high: number;
	low: number;
	close: number;
}

export interface TimeSeriesResponse {
	status: string;
	values: HistoryItem[];
}