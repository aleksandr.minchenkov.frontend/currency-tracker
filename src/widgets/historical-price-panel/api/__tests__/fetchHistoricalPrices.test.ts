import fetchHistoricalPrices from '../fetchHistoricalPrices';
import { IntervalEnum } from '../../model/enums';
import { TimeSeriesResponse } from '../../model/types';

// Mock the getRequest function
jest.mock('shared/api', () => ({
	getRequest: jest.fn(),
}));

describe('fetchHistoricalPrices', () => {
	const { getRequest } = require('shared/api');

	afterEach(() => {
		jest.clearAllMocks();
	});

	it('should fetch historical prices and return the data', async () => {
		const mockResponse: TimeSeriesResponse = {
			status: 'ok',
			values: [
				{ datetime: '2023-01-01', open: 100, close: 110, high: 115, low: 95 },
				{ datetime: '2023-01-02', open: 110, close: 120, high: 125, low: 105 },
			],
		};

		(getRequest as jest.Mock).mockResolvedValue({
			data: mockResponse,
		});

		const stock = 'AAPL';
		const interval = IntervalEnum['1day'];
		const result = await fetchHistoricalPrices(stock, interval);

		expect(getRequest).toHaveBeenCalledWith(`/time_series?symbol=${stock}&interval=${interval}`);
		expect(result).toEqual(mockResponse.values);
	});

	it('should handle errors correctly', async () => {
		const mockError = new Error('Network error');
		(getRequest as jest.Mock).mockRejectedValue(mockError);

		const stock = 'AAPL';
		const interval = IntervalEnum['1day'];

		await expect(fetchHistoricalPrices(stock, interval)).rejects.toThrow('Network error');
		expect(getRequest).toHaveBeenCalledWith(`/time_series?symbol=${stock}&interval=${interval}`);
	});
});
