import { getRequest } from "shared/api";
import { IntervalEnum } from "../model/enums";
import { HistoryItem, TimeSeriesResponse } from "../model/types";

const fetchHistoricalPrices = async (stock: string, interval: IntervalEnum): Promise<HistoryItem[]> => {
	return await getRequest<TimeSeriesResponse>(`/time_series?symbol=${stock}&interval=${interval}`)
		.then((resp) => {
			return resp.data.values;
		})
};

export default fetchHistoricalPrices;