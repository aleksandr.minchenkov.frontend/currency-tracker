import { css } from "@emotion/react";

export const containerStyles = css`
	display: flex;
	flex-direction: column;
	gap: 1rem;
	align-items: flex-start;
`;

export const selectContainerStyles = css`
	display: flex;
	align-items: center;
	gap: 2rem;
`;

export const selectStyles = css`
	width: 100px;
`;