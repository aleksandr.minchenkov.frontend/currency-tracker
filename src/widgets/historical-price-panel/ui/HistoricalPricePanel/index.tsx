import { useEffect, useState } from 'react';
import { Select } from "antd";
import { useQueryParams } from "shared/hooks";
import { HistoryItem } from "../../model/types";
import HistoryTable from "../../ui/HistoryTable";
import { IntervalEnum } from "../../model/enums";
import fetchHistoricalPrices from "../../api/fetchHistoricalPrices";
import { containerStyles, selectContainerStyles, selectStyles } from "./styles";

const HistoricalPricePanel = () => {
	const params = useQueryParams();
	const stock = params.get('stock') ?? '';

	const [historyData, setHistoryData] = useState<HistoryItem[]>([]);
	const [interval, setInterval] = useState(IntervalEnum["1h"]);

	useEffect(() => {
		fetchHistoricalPrices(stock, interval)
			.then(data => setHistoryData(data))
			.catch(error => console.info('error', error));
	}, [interval]);

	return (
		<div css={containerStyles}>
			<h2>Historical data</h2>
			<div css={selectContainerStyles}>
				<span>Choose interval</span>
				<Select
					css={selectStyles}
					value={interval}
					options={Object.values(IntervalEnum).map(interval => ({ value: interval, label: interval}))}
					onChange={(value) => setInterval(value) }
				/>
			</div>
			<HistoryTable data={historyData} />
		</div>
	);
};

export default HistoricalPricePanel;