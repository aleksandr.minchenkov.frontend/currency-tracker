import { useCallback } from 'react';
import { Table } from 'antd';
import { css, useTheme } from "@emotion/react";
import { calculateOpenVsClosePercent, formatPrice } from "shared/lib/price-format";
import { HistoryItem } from "../../model/types";
import type { ColumnsType } from 'antd/es/table';

interface TableProps {
	data: HistoryItem[];
}

const History = ({ data }:TableProps) => {
	const theme = useTheme();

	const getValueColor = useCallback((record: HistoryItem) => {
		if(record.open > record.close) return css`color: ${theme.color.negative}`;
		if(record.open < record.close) return css`color: ${theme.color.positive}`;
		return css``;
	},[]);

	const columns: ColumnsType<HistoryItem> = [
		{
			title: 'Date',
			dataIndex: 'datetime',
			key: 'datetime',
			align: 'center',
			render: (datetime) => <span>{new Date(datetime).toLocaleString()}</span>
		},
		{
			title: 'Open',
			dataIndex: 'open',
			key: 'open',
			align: 'center',
			render: (value) => <span>{formatPrice(value)}</span>,
		},
		{
			title: 'High',
			dataIndex: 'high',
			key: 'high',
			align: 'center',
			render: (value) => <span>{formatPrice(value)}</span>,
		},
		{
			title: 'Low',
			dataIndex: 'low',
			key: 'low',
			align: 'center',
			render: (value) => <span>{formatPrice(value)}</span>,
		},
		{
			title: 'Close',
			dataIndex: 'close',
			key: 'close',
			align: 'center',
			render: (value, record) => <span css={getValueColor(record)}>{formatPrice(value)}</span>
		},
		{
			title: '% Change',
			dataIndex: 'change',
			key: 'change',
			align: 'center',
			render: (_,record) => <span css={getValueColor(record)}>{calculateOpenVsClosePercent(record.open, record.close)}</span>
		},
	];

	return (
		<Table
			dataSource={data}
			columns={columns}
			pagination={false}
			rowKey={(record) => record.datetime}
			scroll={{ y: '60vh' }}
		/>
	);
};

export default History;