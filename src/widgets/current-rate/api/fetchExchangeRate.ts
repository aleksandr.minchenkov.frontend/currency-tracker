import { getRequest } from "shared/api";
import { RateInfo } from "../model/types";

const fetchExchangeRate = async (stock: string): Promise<RateInfo> => {
	return await getRequest<RateInfo>(`/exchange_rate?symbol=${stock}`)
		.then((resp) => {
			return resp.data;
		});
};

export default fetchExchangeRate;