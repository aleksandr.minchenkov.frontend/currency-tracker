import { Dispatch, SetStateAction, useEffect } from "react";
import { config } from "shared/data";
import { CommodityInfo, RateInfo } from "../model/types";

const useStockPriceSubscriptionWS = (stock: string, updateStockInfo: Dispatch<SetStateAction<RateInfo | null>>) => {
	useEffect(() => {
		const socket = new WebSocket(`${config.BASE_WS_URL}/v1/quotes/price?apikey=${config.API_KEY}`);

		socket.onopen = () => {
			socket.send(JSON.stringify({
				"action": "subscribe",
				"params": {
					"symbols": stock
				}}))
		};

		socket.onmessage = (ev) => {
			const message: CommodityInfo = JSON.parse(ev.data);
			if(message.event === 'price') {
				updateStockInfo((oldValue) => (oldValue ? { ...oldValue, rate: message.price, timestamp: message.timestamp } : null))
			}
		}

		return () => {
			socket.close();
		};
	},[]);
}

export default useStockPriceSubscriptionWS;