import { renderHook, act } from '@testing-library/react';
import useStockPriceSubscriptionWS from '../useStockPriceSubscriptionWS';
import WS from 'jest-websocket-mock';

describe('useStockPriceSubscriptionWS', () => {
	let server: WS;

	beforeEach(() => {
		server = new WS('ws://localhost:1234/v1/quotes/price?apikey=apikey');
	});

	afterEach(() => {
		WS.clean();
	});

	it('should subscribe to stock price updates and update stock info on receiving messages', async () => {
		const mockUpdateStockInfo = jest.fn();
		const stock = 'AAPL';

		renderHook(() => useStockPriceSubscriptionWS(stock, mockUpdateStockInfo));

		// Wait for the WebSocket connection to be established
		await server.connected;

		// Check that the correct subscription message was sent
		await expect(server).toReceiveMessage(
			JSON.stringify({
				action: 'subscribe',
				params: {
					symbols: stock,
				},
			})
		);

		// Simulate receiving a message from the WebSocket server
		const mockMessage = {
			event: 'price',
			price: 150,
			timestamp: 1628168237,
		};

		act(() => {
			server.send(JSON.stringify(mockMessage));
		});

		// Verify the updateStockInfo function was called with the correct values
		expect(mockUpdateStockInfo).toHaveBeenCalledWith(expect.any(Function));

		// Check if updateStockInfo updates state correctly
		const updateFunction = mockUpdateStockInfo.mock.calls[0][0];
		const prevState = { rate: 140, timestamp: 1628160000 };
		const updatedState = updateFunction(prevState);

		expect(updatedState).toEqual({
			rate: 150,
			timestamp: 1628168237,
		});

		// Check if updateStockInfo returns null if previous state is null
		const updatedStateNull = updateFunction(null);

		expect(updatedStateNull).toBeNull();
	});

	it('should close the WebSocket connection on unmount', async () => {
		const mockUpdateStockInfo = jest.fn();
		const stock = 'AAPL';

		const { unmount } = renderHook(() => useStockPriceSubscriptionWS(stock, mockUpdateStockInfo));

		// Wait for the WebSocket connection to be established
		await server.connected;

		// Unmount the hook
		unmount();

		// Check that the WebSocket connection is closed
		expect(server.server.clients()[0].readyState).toBe(WebSocket.CLOSING);
	});
});