import fetchExchangeRate from '../fetchExchangeRate';
import { RateInfo } from '../../model/types';

// Mock the getRequest function from 'shared/api'
jest.mock('shared/api', () => ({
	getRequest: jest.fn(),
}));

describe('fetchExchangeRate', () => {
	const { getRequest } = require('shared/api');

	afterEach(() => {
		jest.clearAllMocks();
	});

	it('should fetch exchange rate and return the data', async () => {
		const mockRateInfo: RateInfo = {
			rate: 150,
			symbol: 'AAPL',
			timestamp: 1628168237,
		};

		(getRequest as jest.Mock).mockResolvedValue({
			data: mockRateInfo,
		});

		const stock = 'AAPL';
		const result = await fetchExchangeRate(stock);

		expect(getRequest).toHaveBeenCalledWith(`/exchange_rate?symbol=${stock}`);
		expect(result).toEqual(mockRateInfo);
	});

	it('should handle errors correctly', async () => {
		const mockError = new Error('Network error');
		(getRequest as jest.Mock).mockRejectedValue(mockError);

		const stock = 'AAPL';

		await expect(fetchExchangeRate(stock)).rejects.toThrow('Network error');
		expect(getRequest).toHaveBeenCalledWith(`/exchange_rate?symbol=${stock}`);
	});
});
