import React from 'react';
import { cardContainerStyles, commodityLabelStyles, rateStyles } from "./styles";
import { RateInfo } from "../../model/types";
import { Skeleton } from "antd";

interface Props {
	data: RateInfo | null;
}

const CommodityRateCard = ({ data }:Props) => {
	return (
		<div css={cardContainerStyles}>
			{!data ?
				<React.Fragment>
					<Skeleton />
				</React.Fragment>
				: <React.Fragment>
					<div css={commodityLabelStyles}>{data.symbol}</div>
					<div css={rateStyles}>
						<h2>{data.rate}</h2>
						<span>{data.symbol.split('/')[1]}</span>
					</div>
					<div>Last update: {new Date(data.timestamp * 1000).toLocaleString()}</div>
				</React.Fragment>
			}
		</div>
	);
};

export default CommodityRateCard;