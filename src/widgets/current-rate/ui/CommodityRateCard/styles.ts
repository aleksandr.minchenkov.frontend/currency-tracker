import { css, Theme } from "@emotion/react";

export const cardContainerStyles = (theme: Theme) => css`
  display: flex;
	flex-direction: column;
	padding: 1rem;
	background-color: ${theme.color.background};
	border-radius: 10px;
	min-width: 150px;
	align-items: flex-start;
	gap: 1rem;
`;

export const skeletonStyles = css`
	height: 20px;
`;

export const commodityLabelStyles = (theme: Theme) => css`
	padding: 0.2rem 0.5rem;
	background-color: ${theme.color.border};
	border-radius: 20px;
`;

export const rateStyles = css`
	display: flex;
  align-items: center;
  gap: 0.5rem;
`;