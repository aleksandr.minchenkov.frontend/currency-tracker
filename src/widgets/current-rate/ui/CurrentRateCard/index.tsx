import { useEffect, useState } from 'react';
import { useQueryParams } from "shared/hooks";
import { RateInfo } from "../../model/types";
import CommodityRateCard from "../../ui/CommodityRateCard";
import fetchExchangeRate from "../../api/fetchExchangeRate";
import useStockPriceSubscriptionWS from "../../api/useStockPriceSubscriptionWS";

const CurrentRateCard = () => {
	const params = useQueryParams();
	const stock = params.get('stock') ?? '';

	const [rateInfo, setRateInfo] = useState<RateInfo | null>(null);

	useEffect(() => {
		fetchExchangeRate(stock).then(data => setRateInfo(data))
		.catch(error => console.info('error', error));
	}, []);

	useStockPriceSubscriptionWS(stock, setRateInfo);

	return (
		<div>
			<CommodityRateCard data={rateInfo} />
		</div>
	);
};

export default CurrentRateCard;