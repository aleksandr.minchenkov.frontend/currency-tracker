export interface RateInfo {
	symbol: string;
	timestamp: number;
	rate: number;
}

export interface CommodityInfo {
	event: string;
	symbol: string;
	timestamp: number;
	price: number;
}