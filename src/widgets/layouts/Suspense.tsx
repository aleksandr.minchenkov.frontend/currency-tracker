import { ReactNode, Suspense } from 'react';

interface Props {
	children: ReactNode;
}

const SuspenseWrapper = ({ children }: Props) => {
	return <Suspense fallback={''}>{children}</Suspense>;
};

export default SuspenseWrapper;