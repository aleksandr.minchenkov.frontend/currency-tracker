import { css } from "@emotion/react";

export const rootContainerStyles = css`
  width: 100%;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
	align-items: center;
	gap: 2rem;
`;

export const mainContainerStyles = css`
  display: flex;
  flex-direction: column;
  height: 100%;
  max-width: 1140px;
`;