import { NavLink } from "react-router-dom";
import { css } from "@emotion/react";

const Header = () => {
	return (
		<header css={theme => css`width: 100%; padding: 10px 10px; display: flex; justify-content: center; background-color: ${theme.color.background}`}>
			<div css={css`max-width: 1140px; width: 100%`}>
				<NavLink to={'/'}>Home</NavLink>
			</div>
		</header>
	);
};

export default Header;