import { Outlet } from 'react-router-dom';
import Header from "./Header";
import { mainContainerStyles, rootContainerStyles } from "widgets/layouts/styles";

const Layout = () => {
	return (
		<div css={rootContainerStyles}>
			<Header />
			<main css={mainContainerStyles}>
				<Outlet />
			</main>
		</div>
	);
};

export default Layout;