import { lazy } from 'react';
import { Route, Routes } from 'react-router-dom';
import { PAGE_URLS } from 'shared/data/urls';
import Layout from "../widgets/layouts/Layout";
import Suspense from "../widgets/layouts/Suspense";

const HomePage = lazy(() => import('pages/HomePage'));
const DetailsPage = lazy(() => import('pages/DetailsPage'));


export const routes = [
	{
		path: PAGE_URLS.home,
		name: 'Home',
		component: <HomePage />,
	},
	{
		path: PAGE_URLS.details,
		name: 'Details',
		component: <DetailsPage />,
	},
]

const AppRoutes = () => {
	return (
		<Suspense>
			<Routes>
				<Route
					path={PAGE_URLS.home}
					element={<Layout />}
				>
					{routes.map((route) => {
						return (
							<Route
								key={route.path}
								path={route.path}
								element={route.component}
							/>
						);
					})}
				</Route>
			</Routes>
		</Suspense>
	);
};

export default AppRoutes;