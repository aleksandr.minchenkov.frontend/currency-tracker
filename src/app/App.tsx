import { ThemeProvider } from '@emotion/react'
import { GlobalStyles, theme } from "shared/theme";
import AppRoutes from "./routes";

function App() {
  return (
    <ThemeProvider theme={theme}>
      <GlobalStyles />
      <AppRoutes />
    </ThemeProvider>
  )
}

export default App
