export const calculateOpenVsClosePercent = (open: number, close: number) => {
	const result = ((close - open)/open) * 100;
	return `${(Math.round((result + Number.EPSILON) * 10000) / 10000).toFixed(4)}%`
}

export const formatPrice = (price: number) => {
	if(price >= 1000) {
		const result = price / 1000;
		return `${(Math.round((result + Number.EPSILON) * 100) / 100).toFixed(2)}K`
	}
	return (Math.round((Number(price) + Number.EPSILON) * 10000) / 10000).toFixed(4);
}