import { calculateOpenVsClosePercent, formatPrice } from '../price-format';

describe('calculateOpenVsClosePercent', () => {
	it('should correctly calculate the percentage change from open to close', () => {
		expect(calculateOpenVsClosePercent(100, 110)).toBe('10.0000%');
		expect(calculateOpenVsClosePercent(100, 90)).toBe('-10.0000%');
		expect(calculateOpenVsClosePercent(50, 75)).toBe('50.0000%');
		expect(calculateOpenVsClosePercent(75, 75)).toBe('0.0000%');
	});

	it('should handle edge cases correctly', () => {
		expect(calculateOpenVsClosePercent(0, 100)).toBe('Infinity%');
		expect(calculateOpenVsClosePercent(100, 0)).toBe('-100.0000%');
	});
});

describe('formatPrice', () => {
	it('should correctly format the price under 1000', () => {
		expect(formatPrice(999.9999)).toBe('999.9999');
		expect(formatPrice(0)).toBe('0.0000');
		expect(formatPrice(100)).toBe('100.0000');
	});

	it('should correctly format the price over 1000', () => {
		expect(formatPrice(1000)).toBe('1.00K');
		expect(formatPrice(10000)).toBe('10.00K');
		expect(formatPrice(12345.6789)).toBe('12.35K');
	});

	it('should handle edge cases correctly', () => {
		expect(formatPrice(1000)).toBe('1.00K');
	});
});
