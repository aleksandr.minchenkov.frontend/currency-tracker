import colors from './colors';

const theme = {
	color: {
		text: colors.dark,
		background: colors.background,
		positive: colors.success,
		negative: colors.error,
		white: colors.white,
		dark: colors.dark,
		darkBlue: colors.darkBlue,
		border: colors.border,
	}
};

export default theme;
