import '@emotion/react';

declare module '@emotion/react' {
	export interface Theme {
		color: {
			text: string;
			background: string;
			positive: string;
			negative: string;
			white: string;
			dark: string;
			darkBlue: string;
			border: string;
		}
	}
}