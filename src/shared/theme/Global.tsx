import { Global, useTheme, css } from '@emotion/react'

const GlobalStyles = () => {
	const theme = useTheme()
	return (
		<Global styles={css`
			#root {
				width: 100%;
			}
			
			font-family: Inter, system-ui, Avenir, Helvetica, Arial, sans-serif;
			line-height: 1.5;
			font-weight: 400;

  		color: ${theme.color.text};
  		background-color: ${theme.color.white};

  		font-synthesis: none;
  		text-rendering: optimizeLegibility;
  		-webkit-font-smoothing: antialiased;
  		-moz-osx-font-smoothing: grayscale;
  		
  		body {
  		margin: 0;
			display: flex;
			place-items: flex-start;
			min-width: 320px;
			min-height: 100vh;
			}
			
			h2 {
			margin: 0;
			}
			
			a {
			font-weight: 500;
			color: ${theme.color.dark};
			text-decoration: inherit;
			}
			
			a:hover {
			color: ${theme.color.darkBlue};
			}
			
    `} />
	)
}

export default GlobalStyles;