const colors = {
	dark: '#1C1E28',
	darkBlue: '#1C4CA9',
	alert: '#CF9401',
	alertBack: '#fef2b8',
	medium: '#525970',
	middleBlue: '#3A67BF',
	error: '#CF283C',
	errorBack: '#fde4e4',
	light: '#727B9B',
	lightBlue: '#809FED',
	success: '#59AA21',
	border: '#C3D5F0',
	accent: '#FF6D1C',
	background: '#FAFAFA',
	accentBack: '#FFF1E9',
	white: '#FFFFFF',
};

export default colors;