import config from "./config";
import { PAGE_URLS } from "./urls";

export { config, PAGE_URLS };