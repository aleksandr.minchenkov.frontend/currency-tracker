const config = {
	BASE_REST_URL: import.meta.env.VITE_BASE_REST_URL,
	BASE_WS_URL: import.meta.env.VITE_BASE_WS_URL,
	API_KEY: import.meta.env.VITE_API_KEY,
};

export default config;
