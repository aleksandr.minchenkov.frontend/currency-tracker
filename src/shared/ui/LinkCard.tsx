import { NavLink } from "react-router-dom";
import { css, Theme } from "@emotion/react";

const cardStyles = (theme: Theme) => css`
	padding: 10px;
	border: 1px solid ${theme.color.text};
	background-color: ${theme.color.background};
	border-radius: 10px;
`;

interface LinkCardProps {
	label: string;
	link: string;
}

const LinkCard = ({ label, link } : LinkCardProps) => {
	return (
		<NavLink to={link}>
			<div css={cardStyles}>{label}</div>
		</NavLink>

	);
};

export default LinkCard;