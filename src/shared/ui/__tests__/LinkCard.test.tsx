import { render } from '@testing-library/react';
import { ThemeProvider } from '@emotion/react';
import { BrowserRouter as Router } from 'react-router-dom';
import '@testing-library/jest-dom';
import LinkCard from '../LinkCard';
import { theme } from "shared/theme";

describe('LinkCard', () => {
	it('should render with the correct label and link', () => {
		const { getByText } = render(
			<ThemeProvider theme={theme}>
				<Router>
					<LinkCard label="Test Label" link="/test-link" />
				</Router>
			</ThemeProvider>
		);

		const linkElement = getByText('Test Label');
		expect(linkElement.closest('a')).toHaveAttribute('href', '/test-link');
	});

	it('should apply the correct styles', () => {
		const { getByText } = render(
			<ThemeProvider theme={theme}>
				<Router>
					<LinkCard label="Test Label" link="/test-link" />
				</Router>
			</ThemeProvider>
		);

		const divElement = getByText('Test Label');
		expect(divElement).toHaveStyle(`padding: 10px`);
		expect(divElement).toHaveStyle(`border: 1px solid ${theme.color.text}`);
		expect(divElement).toHaveStyle(`background-color: ${theme.color.background}`);
		expect(divElement).toHaveStyle(`border-radius: 10px`);
	});
});
