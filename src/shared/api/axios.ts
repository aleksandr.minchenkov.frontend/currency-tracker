import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';
import { config } from 'shared/data';

const baseURL = `${config.BASE_REST_URL}`;

const axiosRequests = axios.create({
	baseURL,
});

axiosRequests.interceptors.request.use(
	(config) => {
		config.headers.setAuthorization(`apikey ${import.meta.env.VITE_API_KEY}`);
		return config;
	},
	(error) => Promise.reject(error),
);

const handleResponse = (response: AxiosResponse) => response;

const handleError = async (error: unknown) => {
	console.info('error', error);
	const { message } = error as Error;
	throw new Error(message);
};

axiosRequests.interceptors.response.use(handleResponse, handleError);

async function getRequest<T>(url: string, config?: AxiosRequestConfig): Promise<AxiosResponse<T>> {
	return await axiosRequests.get<T>(url, config);
}

const postRequest = async <T>(url: string, payload: object, config?: AxiosRequestConfig) => {
	return await axiosRequests.post<T>(url, payload, config);
};

const putRequest = async <T>(url: string, payload?: object) => {
	return await axiosRequests.put<T>(url, payload);
};

const patchRequest = async <T>(url: string, payload?: object) => {
	return await axiosRequests.patch<T>(url, payload);
};

const deleteRequest = async (url: string) => {
	return await axiosRequests.delete(url);
};

export { getRequest, postRequest, putRequest, patchRequest, deleteRequest };
