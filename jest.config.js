export default {
    testEnvironment: "jsdom",
    coveragePathIgnorePatterns: [
        "/node_modules/",
    ],
    transform: {
        '^.+\\.tsx?$': [
            'ts-jest',
            {
                diagnostics: {
                    ignoreCodes: [1343]
                },
                astTransformers: {
                    before: [
                        {
                            path: 'node_modules/ts-jest-mock-import-meta',
                            options: { metaObjectReplacement: {
                                env: {
                                    VITE_BASE_REST_URL: 'http://localhost:1234',
                                    VITE_BASE_WS_URL: 'ws://localhost:1234',
                                    VITE_API_KEY: 'apikey',
                                } } }
                        }
                    ]
                }
            }
        ]
    },

    moduleNameMapper: {
        "\\.(css|less|sass|scss)$": "identity-obj-proxy",
        "^.+\\.svg$": "jest-transformer-svg",
        "^widgets/(.*)$": "<rootDir>/src/widgets/$1",
        "^shared/(.*)$": "<rootDir>/src/shared/$1",
    },

    setupFilesAfterEnv: ["<rootDir>/jest.setup.ts"],
};